// See https://github.com/andris9/feedster for dodcumentation
var commithash = process.env.CI_COMMIT_SHA;
var feedster = require('feedster');
var feed = feedster.createFeed({
  title: 'GitLab design pattern library',
  description: 'GitLab design pattern library',
  link: 'https://gitlab.com/dimitrieh/sketch-library-demo',
  image: {
    url: 'https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/wordmark/stacked_wm.png',
    title: 'GitLab design pattern library logo',
    link: 'https://gitlab.com/dimitrieh/sketch-library-demo'
  },
  generator: 'Sketch',
  lastBuildDate: new Date(),
  language: 'en-us',
});
feed.addItem({
  title: 'GitLab design pattern library',
  description: 'GitLab design pattern library',
  guid: {
    value: commithash,
    isPermaLink: false
  },
  pubDate: '2018-01-01 00:00:00',
  link: 'https://gitlab.com/dimitrieh/sketch-library-demo',
  enclosure: {
    url: 'https://dimitrieh.gitlab.io/sketch-library-demo/gitlab-pattern-library.sketch',
    length: 0,
    type: 'application/octet-streams',
    'sparkle:version': '1'
  }
})
var rss = feed.render({ indent: '  ' });
console.log(rss);
